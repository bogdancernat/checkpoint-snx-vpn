# Checkpoint VPN - linux

### Ubuntu installation
- `sudo apt install lib32z1 lib32ncurses5 libstdc++5:i386 libpam0g:i386`
- `sudo su`
- `./snx_install_linux30.sh`

### Usage
- `snx -s server_ip -u username -g` This will start the vpn as a daemon
- to disconnect, run `snx -d`
